package eu.mrpirotech.ymovie.injection

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {


    @Provides
    fun sharedPreferences(
        @ApplicationContext context: Context
    ) = context.getSharedPreferences("app_prefs", Context.MODE_PRIVATE)
}

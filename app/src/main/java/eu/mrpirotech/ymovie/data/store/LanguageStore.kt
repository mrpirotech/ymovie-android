package eu.mrpirotech.ymovie.data.store

import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LanguageStore @Inject constructor(
    private val sharedPreferences: SharedPreferences
) {
    companion object {
        const val SUBTITLES_KEY = "SUBTITLES_KEY"
        const val AUDIO_KEY = "AUDIO_KEY"
    }

    fun getSubtitlesLanguage() = sharedPreferences.getString(SUBTITLES_KEY, null)
    fun getAudioLanguage() = sharedPreferences.getString(AUDIO_KEY, null)

    fun saveSubtitlesLanguage(lang: String?) = sharedPreferences.edit {
        putString(SUBTITLES_KEY, lang)
    }

    fun saveAudioLanguage(lang: String?) = sharedPreferences.edit {
        putString(AUDIO_KEY, lang)
    }
}

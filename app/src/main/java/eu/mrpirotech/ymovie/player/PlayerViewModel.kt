package eu.mrpirotech.ymovie.player

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.media3.common.C
import androidx.media3.common.Tracks
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.mrpirotech.ymovie.data.model.LangSettings
import eu.mrpirotech.ymovie.domain.GetLangSettingsCase
import eu.mrpirotech.ymovie.domain.SaveLangSettingsCase
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PlayerViewModel @Inject constructor(
    private val saveLangSettingsCase: SaveLangSettingsCase,
    private val getLangSettingsCase: GetLangSettingsCase
) : ViewModel() {
    val langSettingsLiveData = MutableLiveData<LangSettings>(null)
    private var isInitialSelection = true

    fun onTracksChanged(tracks: Tracks) {
        if (isInitialSelection) {
            isInitialSelection = false
            loadLangSettings()
            return
        }
        Log.d("XYZ", "Tracks changed")
        saveLangSettings(tracks.groups.find { it.type == C.TRACK_TYPE_TEXT && it.isSelected }
            ?.let { trackGroup ->
                getLanguageFromTrackGroup(trackGroup)
            },
            tracks.groups.find { it.type == C.TRACK_TYPE_AUDIO && it.isSelected }
                ?.let { trackGroup ->
                    getLanguageFromTrackGroup(trackGroup)
                }
        )
    }

    private fun saveLangSettings(subtitle: String?, audio: String?) {
        viewModelScope.launch {
            saveLangSettingsCase(
                SaveLangSettingsCase.Args(subtitleLang = subtitle, audioLang = audio)
            )
        }
    }

    private fun loadLangSettings() {
        viewModelScope.launch {
            getLangSettingsCase(Unit).onSuccess {
                langSettingsLiveData.value = it
            }
        }
    }


    private fun getLanguageFromTrackGroup(trackGroup: Tracks.Group): String? =
        if (trackGroup.length > 0) {
            trackGroup.getTrackFormat(0).language
        } else {
            null
        }


}
